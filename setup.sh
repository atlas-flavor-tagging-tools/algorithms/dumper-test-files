export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

# needed for magic
shopt -s expand_aliases

function getRelease()
{
    local TAG_MATCH='[0-9]+\.[0-9]+\.[0-9]+'
    local T=$(ami show ami-tag $1 | egrep baseRelease | egrep -o $TAG_MATCH )
    if ! egrep -q $TAG_MATCH <<< $T ; then
        echo not a valid tag $T >&2
        return 1
    fi
    echo $T
}

if ! type lsetup &> /dev/null; then
    echo " ===== setup atlas ===="
    . ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
fi

# for some reason we need to set up Athena before AMI
if ! type athena &> /dev/null; then
    asetup --quiet Athena,main,latest
fi

if (( $# >= 1 )); then
    echo " ===== get release ===="
    if ! type ami &> /dev/null; then
        echo "setup ami"
        localSetupPyAMI -q
    fi
    REL=$(getRelease $1)
    echo "got $REL"
    echo " ===== setup release ===="
    asetup --quiet Athena,${REL}
    unset REL
else
    if ! type rucio &> /dev/null; then
        echo " ===== setup rucio ===="
        lsetup "rucio -w" -q
    fi
fi
