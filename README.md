Input files for dumper CI tests
===============================

This repository houses inputs for the training dataset dumper CI tests.
For more information on the creation of test samples please take a look at the training dataset dumper [documentation](https://training-dataset-dumper.docs.cern.ch/test_files/).

To add a new test file run
```
source setup.sh
./add-new-file <dataset-name>
```

### Building from AODs

There's also a script to build a derivation directly from an AOD. You
can run

```
./make-daod <p-tag>
```
.

This should build a new FTAG1 derivation from an input ttbar file
which is stored on CVMFS.
